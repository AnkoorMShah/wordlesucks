﻿using System;
using System.Collections.Generic;
using System.Linq;
using WordleSolverModels;

namespace WordleSolverLogic.Libraries
{
    /// <summary>
    /// Library to handle guess management given state. 
    /// NOTE: This implementation is designed to be as greedy, not smart. Current rules:
    ///     -Identify most common characters of possible choices => pick word to test them.
    ///     -Green > Black > Yellow; maximize tries to get green squares.
    /// </summary>
    internal sealed class GuesserLibrary
    {
        /// <summary>
        /// Guesses what's most likely to be the set of letters to solve the wordle,
        /// then generates a guess to test those letters.
        /// </summary>
        /// <param name="state">Current state maintained by solver.</param>
        /// <param name="allWords">Entire word library.</param>
        internal void GenerateNextGuess(WordleSolverState state, List<string> allWords)
        {
            state.PossibleWords.Remove(state.LatestGuess);

            if (state.PossibleWords.Count == 1)
            {
                state.LatestGuess = state.PossibleWords.Single();
            }
            else
            {
                Dictionary<char, int> characterCountMap = CountCharactersAndGenerateMap(state.PossibleWords);
                char[] requiredCharacters = state.RequiredCharacters.Where(cs => cs.Placed).Select(cs => cs.Character).ToArray();

                // Prioritize non-green letters
                foreach (char requiredCharacter in requiredCharacters)
                {
                    characterCountMap[requiredCharacter] = 0;
                }

                List<string> guessCandidates = GetOptimalWordGuesses(characterCountMap, allWords);
                List<(string, int)> weightedGuessWords = AddWeightToCandidates(state, guessCandidates);
                weightedGuessWords.Sort((a, b) => b.Item2 - a.Item2);
                state.LatestGuess = weightedGuessWords.First().Item1;
            }
        }

        /// <summary>
        /// Choose the best possible choice out of valid word choices remaining.
        /// </summary>
        /// <param name="state">Current state maintained by solver.</param>
        internal void GenerateFinalGuess(WordleSolverState state)
        {
            Dictionary<char, int> characterCountMap = CountCharactersAndGenerateMap(state.PossibleWords);
            List<string> guessCandidates = GetOptimalWordGuesses(characterCountMap, state.PossibleWords);
            List<(string, int)> weightedGuessWords = AddWeightToCandidates(state, guessCandidates);
            weightedGuessWords.Sort((a, b) => b.Item2 - a.Item2);
            state.LatestGuess = weightedGuessWords.First().Item1;
        }

        /// <summary>
        /// Take the results of the guess and adjust state to better approach solution.
        /// </summary>
        /// <param name="state">Current state maintained by solver.</param>
        /// <param name="guessResults">Results from most recent guess attempt.</param>
        internal void UpdateStateWithGuessResults(WordleSolverState state, List<CharacterStatus> guessResults)
        {
            //TODO: Doesn't handle confirmed duplicate characters, maybe add some logic and tweak the state for that

            // The checks have to be in a specific order because of how coloring priority works :/
            for (int g=0; g<guessResults.Count; g++)
            {
                if(guessResults[g] == CharacterStatus.GREEN)
                {
                    UpdateStateForCorrectCharacter(state, g);
                }
            }

            for (int g = 0; g < guessResults.Count; g++)
            {
                if (guessResults[g] == CharacterStatus.YELLOW)
                {
                    UpdateStateForIdentifiedCharacter(state, g);
                }
            }

            for (int g = 0; g < guessResults.Count; g++)
            {
                if (guessResults[g] == CharacterStatus.BLACK)
                {
                    UpdateStateForUnusedCharacter(state, g);
                }
            }

            UpdateStatePossibleWordList(state);
        }



        /// <summary>
        /// Prioritize words that have yellow letters in locations not previously tested.
        /// </summary>
        /// <param name="state">Current state maintained by solver.</param>
        /// <param name="guessCandidates">Best words chosen to solve wordle.</param>
        /// <returns>Weighted list of words.</returns>
        private List<(string, int)> AddWeightToCandidates(WordleSolverState state, List<string> guessCandidates)
        {
            Dictionary<string, int> weightedGuesses = guessCandidates.ToDictionary(g => g, g => 6);
            IEnumerable<CharacterState> identifiedCharacters = state.RequiredCharacters.Where(cs => !cs.Placed);

            foreach (string word in guessCandidates)
            {
                foreach (CharacterState characterState in identifiedCharacters)
                {
                    if (characterState.TestedLocations.Contains(word.IndexOf(characterState.Character)))
                    {
                        weightedGuesses[word]--;
                    }
                }
            }

            return weightedGuesses.Select(wg => (wg.Key, wg.Value)).ToList();
        }

        /// <summary>
        /// Gets a count of all characters in possible word pool.
        /// </summary>
        /// <param name="wordPool">List of words to count characters for.</param>
        /// <returns></returns>
        private Dictionary<char, int> CountCharactersAndGenerateMap(List<string> wordPool)
        {
            //TODO: count characters for EACH position. Choose guess best fitting positional character count.
            Dictionary<char, int> characterCountMap = new Dictionary<char, int>();

            foreach(string word in wordPool)
            {
                foreach(char character in word.ToCharArray())
                {
                    if(characterCountMap.ContainsKey(character))
                    {
                        characterCountMap[character]++;
                    }
                    else
                    {
                        characterCountMap.Add(character, 1);
                    }
                }
            }

            return characterCountMap;
        }

        /// <summary>
        /// Choose a word that would best test for the desired characters given their
        /// priority.
        /// </summary>
        /// <param name="state">Current state maintained by solver.</param>
        /// <param name="characterPriorityMap">
        ///     Map a character to how likely it is in the word
        /// </param>
        /// <returns>Words that best tests for most likely characters</returns>
        private List<string> GetOptimalWordGuesses(Dictionary<char, int> characterCountMap, List<string> allWords)
        {
            List<string> guessCandidates = new List<string>(allWords);
            List<(char, int)> characterPriorityMap = characterCountMap.Select(kv => (kv.Key, kv.Value)).ToList();

            characterPriorityMap.Sort((a, b) => b.Item2 - a.Item2);
            for (int l = 0; l < characterPriorityMap.Count; l++)
            {
                List<string> foundCandidates = guessCandidates.Where(w => w.Contains(characterPriorityMap[l].Item1)).ToList();
                if (foundCandidates.Any())
                {
                    guessCandidates = foundCandidates;
                }
            }

            return guessCandidates;
        }

        /// <summary>
        /// Update possible characters for a position. Ensure it is marked as a confirmed 
        /// required character. 
        /// NOTE: this can eliminate up to 25 characters per call!
        /// </summary>
        /// <param name="state">Current state maintained by solver.</param>
        /// <param name="guessedCharIndex">Character that is in the correct spot.</param>
        private void UpdateStateForCorrectCharacter(WordleSolverState state, int guessedCharIndex)
        {
            char correctCharacter = state.LatestGuess.ToCharArray()[guessedCharIndex];
            IEnumerable<CharacterState> foundCharacterState = state.RequiredCharacters
                .Where(cs => cs.Character == correctCharacter);

            state.PositionStates[guessedCharIndex].PossibleCharacters = new List<char> { correctCharacter };
            if (foundCharacterState.Any())
            {
                CharacterState characterState = foundCharacterState.Single();
                characterState.Placed = true;
                characterState.TestedLocations.Add(guessedCharIndex);
            }
            else
            {
                //TODO: optimize to set flag to update possible word list regarding required characters
                CharacterState characterState = new CharacterState()
                {
                    Placed = true,
                    Character = correctCharacter
                };

                characterState.TestedLocations.Add(guessedCharIndex);
                state.RequiredCharacters.Add(characterState);
            }
        }

        /// <summary>
        /// Update possible characters for a position. Ensure it is marked as a 
        /// required character with an unknown position.
        /// NOTE: this can eliminate up to 1 character per call!
        /// </summary>
        /// <param name="state">
        ///     Current state maintained by solver. Assumes G letters have been 
        ///     already processed.
        /// </param>
        /// <param name="guessedCharIndex">Character that is in the incorrect spot.</param>
        private void UpdateStateForIdentifiedCharacter(WordleSolverState state, int guessedCharIndex)
        {
            char identifiedCharacter = state.LatestGuess.ToCharArray()[guessedCharIndex];
            IEnumerable<CharacterState> foundCharacterState = state.RequiredCharacters
                .Where(cs => cs.Character == identifiedCharacter);

            state.PositionStates[guessedCharIndex].PossibleCharacters.Remove(identifiedCharacter);
            if (foundCharacterState.Any())
            {
                CharacterState characterState = foundCharacterState.Single();
                characterState.TestedLocations.Add(guessedCharIndex);
            }
            else 
            {
                //TODO: optimize to set flag to update possible word list regarding required characters
                CharacterState characterState = new CharacterState()
                {
                    Placed = false,
                    Character = identifiedCharacter
                };

                characterState.TestedLocations.Add(guessedCharIndex);
                state.RequiredCharacters.Add(characterState);
            }
        }

        /// <summary>
        /// Mark character in state as unused.
        /// NOTE: this can eliminate up to 5 characters per call!
        /// </summary>
        /// <param name="state">
        ///     Current state maintained by solver. Assumes G/Y letters have been 
        ///     already processed.
        /// </param>
        /// <param name="guessedCharIndex">Character that is not a part of the solution.</param>
        private void UpdateStateForUnusedCharacter(WordleSolverState state, int guessedCharIndex)
        {
            char unusedCharacter = state.LatestGuess.ToCharArray()[guessedCharIndex];
            
            // Account for duplicate char being Y/G before, or G after the current index.
            if(state.RequiredCharacters.Select(rc => rc.Character).Contains(unusedCharacter))
            {
                state.PositionStates[guessedCharIndex].PossibleCharacters.Remove(unusedCharacter);
            }
            else
            {
                foreach (PositionState positionState in state.PositionStates)
                {
                    positionState.PossibleCharacters.Remove(unusedCharacter);
                }
            }
        }

        /// <summary>
        /// Update state's possible word list given other state conditions.
        /// </summary>
        /// <param name="state">Current state maintained by solver.</param>
        private void UpdateStatePossibleWordList(WordleSolverState state)
        {
            char[] requiredCharacters = state.RequiredCharacters.Select(cs => cs.Character).ToArray();
            state.PossibleWords = state.PossibleWords
                .Where(w => WordContainsAllCharacters(w, requiredCharacters))
                .ToList();

            for (int ps = 0; ps < state.PositionStates.Count; ps++)
            {
                state.PossibleWords = state.PossibleWords
                    .Where(w => state.PositionStates[ps].PossibleCharacters
                        .Contains(w.ToCharArray()[ps]))
                    .ToList();
            }
        }

        /// <summary>
        /// Helper function to check if a word contains all of a list of characters.
        /// </summary>
        /// <param name="word">Word to test.</param>
        /// <param name="requiredCharacters">Array of characters required in word.</param>
        /// <returns></returns>
        private static bool WordContainsAllCharacters(string word, char[] requiredCharacters)
        {
            bool wordContainsAll = true;
            int requiredCharacterIndex = 0;

            while(wordContainsAll && requiredCharacterIndex < requiredCharacters.Length)
            {
                if(!word.Contains(requiredCharacters[requiredCharacterIndex]))
                {
                    wordContainsAll = false;
                }
                else
                {
                    requiredCharacterIndex++;
                }
            }

            return wordContainsAll;
        }
    }
}