﻿using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace WordleSolverLogic.Libraries
{
    /// <summary>
    /// Library to handle file IO related to word pool management.
    /// </summary>
    internal sealed class WordLoaderLibrary
    {
        /// <summary>
        /// Load appropriate file's lines into a list of words.
        /// </summary>
        /// <returns>List of words</returns>
        internal List<string> LoadWords()
        {
            ////return File.ReadAllLines("Resources/all5LetterWords.txt").Select(w => w.ToUpper()).ToList();
            return File.ReadAllLines("Resources/EnhancedAll5LetterWordsList.txt").Select(w => w.ToUpper()).ToList();
        }
    }
}