﻿using System.Collections.Generic;
using WordleSolverLogic.Libraries;

namespace WordleSolverLogic
{
    /// <summary>
    /// Domain to read non-code assets.
    /// </summary>
    public sealed partial class WordleSolverDomain
    {
        /// <summary>
        /// Exact number of characters allowed in a word.
        /// //TODO: retrieve from a config file
        /// </summary>
        public const int WORD_LENGTH = 5;

        /// <summary>
        /// Library handling File IO for words.
        /// </summary>
        private WordLoaderLibrary WordLoader { get; set; } = new WordLoaderLibrary();
        
        /// <summary>
        /// Static list of entire word pool.
        /// </summary>
        private List<string> AllWords
        {
            get
            {
                if (_allWords == null)
                {
                    _allWords = WordLoader.LoadWords();
                }
                return _allWords;
            }
        }
        private List<string> _allWords = null;



        /// <summary>
        /// Return entire word pool.
        /// </summary>
        /// <returns></returns>
        public List<string> LoadWords()
        {
            return new List<string>(AllWords);
        }
    }
}
