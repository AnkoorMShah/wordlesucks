﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WordleSolverLogic.Libraries;
using WordleSolverModels;

namespace WordleSolverLogic
{
    /// <summary>
    /// Domain methods to maintain state
    /// </summary>
    public sealed partial class WordleSolverDomain
    {
        /// <summary>
        /// Maximum tries allowed by wordle game.
        /// //TODO: retrieve from a config file
        /// </summary>
        public const int MAX_TRIES = 6;

        /// <summary>
        /// Library to handle various forms of guessing.
        /// </summary>
        private GuesserLibrary Guesser { get; set; } = new GuesserLibrary();



        /// <summary>
        /// Examine current state, and greedily determine best word to isolate answer.
        /// </summary>
        /// <param name="state">Current state maintained by solver.</param>
        /// <returns>string word to best isolate correct wordle answer.</returns>
        public void GenerateNextGuess(WordleSolverState state)
        {
            Guesser.GenerateNextGuess(state, AllWords);
            AllWords.Remove(state.LatestGuess);
        }

        /// <summary>
        /// Instead of choosing the best word to isolate the answer, chooses only a 
        /// valid word option.
        /// </summary>
        /// <param name="state">Current state maintained by solver.</param>
        /// <returns>Valid word option most likely to be the correct answer.</returns>
        public void GenerateFinalGuess(WordleSolverState state)
        {
            Guesser.GenerateFinalGuess(state);
        }

        /// <summary>
        /// Update all aspects of the state given the feedback on the latest guess.
        /// </summary>
        /// <param name="state">Current state maintained by solver.</param>
        /// <param name="guessResults">
        ///     List of Characters and their statuses, (green letters and yellow letters)
        /// </param>
        public void UpdateStateWithGuessResults(WordleSolverState state, List<CharacterStatus> guessResults)
        {
            Guesser.UpdateStateWithGuessResults(state, guessResults);
        }
    }
}
