﻿using System.Collections.Generic;

namespace WordleSolverModels
{
    public enum CharacterStatus
    {
        BLACK = 0,
        GREEN = 1,
        YELLOW = 2
    }



    /// <summary>
    /// Metadata about characters that need to be kept in memory.
    /// </summary>
    public sealed class CharacterState
    {
        /// <summary>
        /// If the appropriate position for this character has been identified.
        /// </summary>
        public bool Placed { get; set; } = false;
        /// <summary>
        /// Character who's state is being kept in memory.
        /// </summary>
        public char Character { get; set; }
        /// <summary>
        /// Track tested locations for a required character. Mostly relevent to an 
        /// unplaced character.
        /// </summary>
        public List<int> TestedLocations { get; set; } = new List<int>();
    }
}