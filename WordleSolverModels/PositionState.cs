﻿using System.Collections.Generic;
using System.Linq;

namespace WordleSolverModels
{
    /// <summary>
    /// Maintains info tied to each position in the wordle solution.
    /// </summary>
    public sealed class PositionState
    {
        /// <summary>
        /// Each character still valid for this position.
        /// </summary>
        public List<char> PossibleCharacters { get; set; } = "ABCDEFGHIJKLMNOPQRSTUVWXYZ".ToList();
    }
}