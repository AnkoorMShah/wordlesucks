﻿using System;
using System.Collections.Generic;

namespace WordleSolverModels
{
    /// <summary>
    /// Overall state of wordle solving system.
    /// </summary>
    public sealed class WordleSolverState
    {
        /// <summary>
        /// List of valid characters for each possible position.
        /// </summary>
        public List<PositionState> PositionStates { get; set; }
        /// <summary>
        /// Loaded dictionary of all possible words.
        /// </summary>
        public List<string> PossibleWords { get; set; }
        /// <summary>
        /// Maintained metadata of characters identified as a part of the solution.
        /// </summary>
        public List<CharacterState> RequiredCharacters { get; set; }
        /// <summary>
        /// The most recent guess suggested by the system.
        /// </summary>
        public string LatestGuess { get; set; }
    }
}
