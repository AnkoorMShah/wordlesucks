﻿using System;
using System.Collections.Generic;
using System.Linq;
using WordleSolverLogic;
using WordleSolverModels;

namespace WordleSolver
{
    class Program
    {
        /*************************************************************************************************
        * WORDLE SOLVER (Console version)
        * 
        * NOTE: there is no error checking, as this console version is a place holder to test the solving
        * API. This console app is not meant to handle real user input.
        * 
        * NOTE: This console app does not care if it succeeded. You cannot report to it if it was correct.
        *************************************************************************************************/
        static void Main(string[] args)
        {
            WordleSolverDomain wordleSolver = new WordleSolverDomain();
            WordleSolverState state = InitializeState(wordleSolver);

            for (int t = 0; t < WordleSolverDomain.MAX_TRIES - 1; t++)
            {
                wordleSolver.GenerateNextGuess(state);
                Console.WriteLine($"Guess: {state.LatestGuess}");
                Console.Write("0 (Black) / 1 (Green) / 2 (Yellow) string (e.g. 'BBGYB' => '00120'): ");
                List<CharacterStatus> guessResults =
                    Console.ReadLine()
                    .ToCharArray()
                    .Select(c => (CharacterStatus)int.Parse(c.ToString()))
                    .ToList();

                wordleSolver.UpdateStateWithGuessResults(state, guessResults);
            }

            wordleSolver.GenerateFinalGuess(state);
            Console.WriteLine($"\nMy best guess at this point is: {state.LatestGuess}");
        }



        /// <summary>
        /// Helper function to initialize state to solve the game.
        /// </summary>
        /// <param name="wordleSolver">Domain instance to help build state.</param>
        /// <returns>Initial state of solver.</returns>
        private static WordleSolverState InitializeState(WordleSolverDomain wordleSolver)
        {
            List<PositionState> positionStates = new List<PositionState>();
            for (int p = 0; p < WordleSolverDomain.WORD_LENGTH; p++)
            {
                positionStates.Add(new PositionState());
            }
            WordleSolverState state = new WordleSolverState()
            {
                PositionStates = positionStates,
                PossibleWords = wordleSolver.LoadWords(),
                RequiredCharacters = new List<CharacterState>()
            };
            return state;
        }
    }
}
