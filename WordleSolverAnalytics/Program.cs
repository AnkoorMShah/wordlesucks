﻿using System;
using System.Collections.Generic;
using System.Linq;
using WordleSolverLogic;
using WordleSolverModels;

namespace WordleSolverAnalytics
{
    internal class Program
    {
        /*************************************************************************************************
        * WORDLE SOLVER (Console Analytics version)
        * 
        * Test every word for how many guesses it takes
        *************************************************************************************************/
        static void Main(string[] args)
        {
            List<string> allWords = new List<string>(new WordleSolverDomain().LoadWords());
            List<double> guessCounts = new List<double>();
            int failedCount = 0;
            double totalWinGuessCounts = 0;

            foreach (string word in allWords)
            {
                double guessCount = SolveWordle(word);
                Console.WriteLine($"Word: {word}   |   Guesses: {(int)guessCount}");
                guessCounts.Add(guessCount);
            }
            failedCount = guessCounts.RemoveAll(s => s == -1.0);
            totalWinGuessCounts = guessCounts.Sum();

            Console.WriteLine($"\n\nTotal Tests: {allWords.Count()}\nFailed Count: {failedCount}\nAverage number of guesses: {totalWinGuessCounts / allWords.Count} \n\n");
        }



        /// <summary>
        /// Helper function to initialize state to solve the game.
        /// </summary>
        /// <param name="wordleSolver">Domain instance to help build state.</param>
        /// <returns>Initial state of solver.</returns>
        private static WordleSolverState InitializeState(WordleSolverDomain wordleSolver)
        {
            List<PositionState> positionStates = new List<PositionState>();
            for (int p = 0; p < WordleSolverDomain.WORD_LENGTH; p++)
            {
                positionStates.Add(new PositionState());
            }
            WordleSolverState state = new WordleSolverState()
            {
                PositionStates = positionStates,
                PossibleWords = wordleSolver.LoadWords(),
                RequiredCharacters = new List<CharacterState>()
            };
            return state;
        }

        /// <summary>
        /// Compare guess to solution and generate colors for letters.
        /// </summary>
        /// <param name="wordSolution">Wordle answer.</param>
        /// <param name="latestGuess">Word to test against anaswer.</param>
        /// <returns>List of colors based on comparison.</returns>
        private static List<CharacterStatus> ProcessGuess(string wordSolution, string latestGuess)
        {
            List<CharacterStatus> characterStatuses = new List<CharacterStatus>();

            for(int ci = 0; ci < wordSolution.Length; ci ++)
            {
                char currentChar = latestGuess[ci];
                int wordSolutionCharFrequency = wordSolution.Where(c => c == currentChar).Count();
                int latestGuessCharFrequency = latestGuess.Substring(0, ci + 1).Where(c => c == latestGuess[ci]).Count();

                if (wordSolution[ci] == currentChar)
                {
                    characterStatuses.Add(CharacterStatus.GREEN);
                }
                else if (wordSolutionCharFrequency >= latestGuessCharFrequency)
                {
                    characterStatuses.Add(CharacterStatus.YELLOW);
                }         
                else
                {
                    characterStatuses.Add(CharacterStatus.BLACK);
                }
            }
            
            return characterStatuses;
        }

        /// <summary>
        /// Simulate the wordle game for the provided word.
        /// </summary>
        /// <param name="wordSolution">Word we are playing the wordle game for.</param>
        /// <returns>Number of tries it took, or -1 if we failed.</returns>
        private static double SolveWordle(string wordSolution)
        {
            WordleSolverDomain wordleSolver = new WordleSolverDomain();
            WordleSolverState state = InitializeState(wordleSolver);
            int guessCount = 0;
            bool success = false;

            while (!success && guessCount < WordleSolverDomain.MAX_TRIES - 1)
            {
                wordleSolver.GenerateNextGuess(state);
                guessCount++;
                if (state.LatestGuess == wordSolution)
                {
                    success = true;
                }
                else
                {
                    List<CharacterStatus> guessResults = ProcessGuess(wordSolution, state.LatestGuess);
                    wordleSolver.UpdateStateWithGuessResults(state, guessResults);
                }
            }

            if (!success)
            {
                wordleSolver.GenerateFinalGuess(state);
                guessCount++;
                success = state.LatestGuess == wordSolution;
            }

            return success ? guessCount : -1.0;
        }

    }
}
